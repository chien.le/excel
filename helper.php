<?php

/**
 * Description of helpercalculate
 *
 * @author Le Xuan Chien <chien.lexuan@gmail.com>
 */
class HelperCalculate {

    private $postcode = 3145; //ma buu chinh lay tu truong client_postcode trong bang solar_users
    private $quantity = 20; //so luong san pham chinh lay tu truong product_quantity trong bang solar_quote_products
    private $power = 250; //cong suat moi tam lay tu truong power trong bang solar_quote_products
    private $installer_sun_direction = 'south'; //huong mat troi lay tu truong installer_sun_direction trong bang solar_quote_client_installer
    private $installer_slope = 35; // do nghieng cai dat lay tu truong installer_slope trong bang solar_quote_client_installer
    private $inverter_efficiency = 0.978; //hieu suat bien tan lay tu truong inverter_efficiency trong bang solar_quote_products
    private $terminal_voltage = 33; //dien cap dau cuoi lay tu truong terminal_voltage trong bang solar_quote_products
    private $product_length = 10; //chieu dai cap DC Caple lay tu truong product_length trong bang solar_quote_products
    private $cable_in_mm = 4; //duong kinh cua cap lay tu truong cable_in_mm trong bang solar_quote_products
    private $extraFactor = 0.9; //He so bo sung duoc cau hinh trong Admin lay tu bang solar_configs
    private $shade = 0; //Ti le bong ram lay tu truong shade trong bang solar_quote_client_installer
    private $yearly_increase = 0.03; //lay tu truong config_value theo config_key= yearly_increase trong bang solar_configs
    private $price_per_kwh = 0.22; //lay tu truong config_value theo config_key=price_per_kwh trong bang solar_configs
    private $total_all = -7382.89; //lay tu truong total_all trong bang solar_quotes

    /**
     * Khoi tao cac thuoc tinh
     * @param type $data_calculate
     */

    public function __construct($data_calculate) {
        $this->postcode = $data_calculate['postcode'];
        $this->quantity = $data_calculate['quantity'];
        $this->power = $data_calculate['power'];
        $this->installer_sun_direction = $data_calculate['installer_sun_direction'];
        $this->installer_slope = $data_calculate['installer_slope'];
        $this->inverter_efficiency = $data_calculate['inverter_efficiency'];
        $this->terminal_voltage = $data_calculate['terminal_voltage'];
        $this->product_length = $data_calculate['product_length'];
        $this->cable_in_mm = $data_calculate['cable_in_mm'];
        $this->extraFactor = $data_calculate['extraFactor'];
        $this->shade = $data_calculate['shade'];
        $this->yearly_increase = $data_calculate['yearly_increase'];
        $this->price_per_kwh = $data_calculate['price_per_kwh'];
        $this->total_all = $data_calculate['total_all'];
    }

    /**
     * Ham tinh tong cong suat cua tat ca cac tam
     * @param type $quantity
     * @param type $power
     * @return type
     */
    public function totalPower() {
        return $this->quantity * $this->power;
    }

    /**
     * Tim he so chieu xa theo vi tri, vung mien
     * @param type $postcode
     */
    public function irradiationOnLocation() {
//        $db = JFactory::getDbo();
//        $query = "SELECT `instraling_in_m` FROM " . StringTable::TABLE_SOLAR_POSTCODE . " WHERE `postcode`='" . $this->postcode . "'";
//        $db->setQuery($query);
//        $result = $db->loadResult();
//        if ($result != '' && $result != null) {
//            return $result;
//        } else {
//            return 0;
//        }
        return 1055.64;
    }

    /**
     * Tim he so chieu xa theo do nghieng va huong
     * @param type $installer_slope
     * @param type $installer_sun_direction
     */
    public function irradiationFactor() {
//        $db = JFactory::getDbo();
//        $query = "SELECT `irradiationfactor` FROM " . StringTable::TABLE_SOLAR_IRRADIATIONFACTOR . " WHERE `degree`='" . $this->installer_slope . "' AND `direction`='" . $this->installer_sun_direction;
//        $db->setQuery($query);
//        $result = $db->loadResult();
//        if ($result != '' && $result != null) {
//            return $result;
//        } else {
//            return 0;
//        }
        return 100 / 100;
    }

    /**
     * Tinh he so hieu chinh
     * @param type $irradiationOnLocation
     * @param type $irradiationFactor
     * @return type
     */
    public function correctionFactor() {
        $irradiationOnLocation = $this->irradiationOnLocation();
        return ($irradiationOnLocation * $this->extraFactor) / 1000;
    }

    /**
     * Tong san luong cua tam sau khi nhan voi tong cong suat
     * @param type $totalPower
     * @param type $correctionFactor
     * @return type
     */
    public function totalPowerOfPanel() {
        $correctionFactor = $this->correctionFactor();
        $totalPower = $this->totalPower();
        return $totalPower * $correctionFactor;
    }

    /**
     * Tong san luong sau khi tinh toan den huong do doc
     * @param type $totalPowerOfPanel
     * @param type $irradiationFactor
     * @return type
     */
    public function afterIrradiation() {
        $totalPowerOfPanel = $this->totalPowerOfPanel();
        $irradiationFactor = $this->irradiationFactor();
        return $totalPowerOfPanel * $irradiationFactor;
    }

    /**
     * Phan tram tong san luong dien nang
     * @param type $afterIrradiation
     * @param type $totalPower
     * @return type
     */
    public function percentageOfTotalPower() {
        $afterIrradiation = $this->afterIrradiation();
        $totalPower = $this->totalPower();
        return $afterIrradiation / $totalPower;
    }

    /**
     * Thiet hai do bien tan
     * @param type $afterIrradiation
     * @param type $inverter_efficiency
     * @return type
     */
    public function afterInverterLosses() {
        $afterIrradiation = $this->afterIrradiation();
        return $afterIrradiation * $this->inverter_efficiency;
    }

    /**
     * ti le phan tram bien tan
     * @param type $afterInverterLosses
     * @param type $totalPower
     * @return type
     */
    public function percentageInverter() {
        $afterInverterLosses = $this->afterInverterLosses();
        $totalPower = $this->totalPower();
        return $afterInverterLosses / $totalPower;
    }

    /**
     * Dien nang tao ra
     * @param type $power
     * @param type $percentageOfTotalPower
     * @return type
     */
    public function generatedPower() {
        $percentageOfTotalPower = $this->percentageOfTotalPower();
        return $this->power * $percentageOfTotalPower;
    }

    /**
     * Cuong do dong dien
     * @param type $generatedPower
     * @param type $terminal_voltage
     * @return type
     */
    public function amperage() {
        $generatedPower = $this->generatedPower();
        return $generatedPower / $this->terminal_voltage;
    }

    /**
     * Suc de khang cua caple
     * @param type $cable_in_mm
     * @return type
     */
    public function resistanceCablePerMeter() {
        $cable_in_mm = $this->cable_in_mm;
        return (17 * pow(10, (-9)) / $cable_in_mm) * 2000000;
    }

    /**
     * Dien nang thiet hai cua caple
     * @param type $product_length
     * @param type $resistanceCablePerMeter
     * @param type $amper
     * @return type
     */
    public function powerLoss() {
        $product_length = $this->product_length;
        $resistanceCablePerMeter = $this->resistanceCablePerMeter();
        $amperage = $this->amperage();
        return $product_length * $resistanceCablePerMeter * pow($amperage, 2);
    }

    /**
     * Phan tram dien nang thiet hai capble
     * @param type $powerLoss
     * @param type $generatedPower
     * @return type
     */
    public function powerLossInPercent() {
        $powerLoss = $this->powerLoss();
        $generatedPower = $this->generatedPower();
        return ($powerLoss / $generatedPower) * 100;
    }

    /**
     * Dien nang thiet hai cua cable
     * @param type $afterInverterLosses
     * @param type $powerLoss
     * @return type
     */
    public function afterCableLosses() {
        $afterInverterLosses = $this->afterInverterLosses();
        $powerLoss = $this->powerLoss();
        return $afterInverterLosses - $powerLoss;
    }

    /**
     * Phan tram doanh thu cua solarsystem
     * @param type $afterCableLosses
     * @param type $totalPower
     * @return type
     */
    public function percentageRevenue() {
        $afterCableLosses = $this->afterCableLosses();
        $totalPower = $this->totalPower();
        return $afterCableLosses / $totalPower;
    }

    /**
     * Tong so dien nang
     * @param type $afterCableLosses
     * @param type $shade
     * @return type
     */
    public function totalPowerInKWh() {
        $afterCableLosses = $this->afterCableLosses();
        $shade = $this->shade;
        return round($afterCableLosses - ($shade * $afterCableLosses), 2);
    }

    /**
     * Tong so ti le phan tram dien nang
     * @param type $totalPowerInKWh
     * @param type $totalPower
     * @return type
     */
    public function totalRevenueInPercent() {
        $totalPowerInKWh = $this->totalPowerInKWh();
        $totalPower = $this->totalPower();
        return $totalPowerInKWh / $totalPower;
    }

    /**
     * Ham tinh tong dien nang moi thang phuc vu cho bieu do dien nang theo thang trong Quote output
     * @param type $totalPowerInKWh
     * @return type
     */
    public function revenuePerMonth() {
        $totalPowerInKWh = $this->totalPowerInKWh();
        $arrPermonth = array();
        for ($i = 1; $i <= 12; $i++) {
            $arrPermonth[$i] = round($totalPowerInKWh / 100 * PerMonth::$arr_per_month[$i - 1], 1);
        }
        return $arrPermonth;
    }

    /**
     * Ham tinh gia tri phuc vu cho bieu do ROI
     * @param type $payback_time
     * @param type $cumulative_revenue
     * @param type $arr_yearly_increase
     * @param type $arr_price_per_kwh
     * @param type $arr_factor_loss
     * @param type $decreasing_revenue
     * @param type $revene_per_year
     * @return type
     */
    public function roiForCustomer(&$payback_time, &$cumulative_revenue, &$arr_yearly_increase = null, &$arr_price_per_kwh = null, &$arr_factor_loss = null, &$decreasing_revenue = null, &$revene_per_year = null) {
        $yearly_increase = $this->yearly_increase;
        $price_per_kwh = $this->price_per_kwh;
        $totalPowerInKWh = $this->totalPowerInKWh();
        $total_all = $this->total_all;
        $arr_yearly_increase = array();
        $arr_price_per_kwh = array();
        $arr_factor_loss = array();
        $decreasing_revenue = array();
        $revene_per_year = array();
        $cumulative_revenue = array();
        $winst = array();
        $payback_time = array();
        $arr_temp_price_per_kwh = array();
        $arr_temp_factor_loss = array();
        $arr_temp_decreasing_revenue = array();
        $arr_temp_revene_per_year = array();
        $arr_temp_cumulative_revenue = array();
        for ($i = 1; $i <= 25; $i++) {
            if ($i == 1) {
                $arr_yearly_increase[1] = 1;
                $arr_price_per_kwh[1] = $price_per_kwh;
                $arr_factor_loss[1] = 1;
                $arr_temp_price_per_kwh[1] = $arr_price_per_kwh[1];
                $arr_temp_factor_loss[1] = $arr_factor_loss[1];
            } else {
                $arr_yearly_increase[$i] = round(pow((1 + $yearly_increase), $i - 1), 2);
                $arr_temp_price_per_kwh[$i] = $arr_temp_price_per_kwh[$i - 1] * (1 + $yearly_increase);
                $arr_price_per_kwh[$i] = round($arr_temp_price_per_kwh[$i], 2);
                $arr_temp_factor_loss[$i] = $arr_temp_factor_loss[$i - 1] - (0.2 / 24);
                $arr_factor_loss[$i] = round($arr_temp_factor_loss[$i], 3);
            }
            $arr_temp_decreasing_revenue[$i] = $totalPowerInKWh * $arr_temp_factor_loss[$i];
            $decreasing_revenue[$i] = round($arr_temp_decreasing_revenue[$i]);
            $arr_temp_revene_per_year[$i] = $arr_temp_decreasing_revenue[$i] * $arr_temp_price_per_kwh[$i];
            $revene_per_year[$i] = round($arr_temp_revene_per_year[$i], 2);
            if ($i == 1) {
                $cumulative_revenue[1] = $revene_per_year[1];
                $arr_temp_cumulative_revenue[1] = $cumulative_revenue[1];
                $winst[1] = $total_all + $revene_per_year[1];
            } else {
                $arr_temp_cumulative_revenue[$i] = $arr_temp_cumulative_revenue[$i - 1] + $arr_temp_revene_per_year[$i];
                $cumulative_revenue[$i] = round($arr_temp_cumulative_revenue[$i], 2);
                $winst[$i] = $winst[$i - 1] + $revene_per_year[$i];
                if (($winst[$i - 1] < 0) && ($winst[$i] > 0)) {
                    $payback_time[$i - 1] = round(($i - 1) + ((-$winst[$i - 1]) / $revene_per_year[$i - 1]), 2);
                } else {
                    $payback_time[$i - 1] = 0;
                }
                if ($i == 25) {
                    if ($winst[$i] < 0) {
                        $payback_time = round(($i) + ((-$winst[$i]) / $revene_per_year[$i]), 2);
                    } else {
                        $payback_time[$i] = 0;
                    }
                }
            }
        }
        return $winst;
    }

}

?>
