<?php
/**
 * Description of class
 * Lớp kết nối và truy vấn Database
 * @author chienlx
 */
class DB {

    function __construct() {
        
    }

    /**
     * Hàm kết nối tới Database
     * @staticvar null $connect
     * @param string $host
     * @param string $database
     * @param string $username
     * @param string $pass
     * @return boolean 
     */
    public function connectDB($host, $database, $username, $pass) {
        static $connect = NULL;
        if ($connect == NULL) {
            try {
                $connect = mysql_connect($host, $username, $pass);
                mysql_select_db($database);
            } catch (Exception $exc) {
                die("Lỗi kết nối Database:" . $exc->getMessage());
            }
        }
        return $connect;
    }

    /**
     * Hàm đóng kết nối Database
     */
    public function closeDB() {
        mysql_close();
    }

    /**
     * Hàm truy vấn trả về số lượng bản ghi của 1 bảng bất kỳ
     * @param string $nametable: Tên bảng
     * @param string $nameid: Tên trường khóa chính của bảng
     * @return int 
     */
    public function queryReturnCount($nametable, $nameid) {
        $query = "SELECT COUNT(" . $nameid . ") AS count FROM " . $nametable;
        try {
            $result = mysql_query($query);
            $row = mysql_fetch_assoc($result);
            $count = $row['count'];
            return $count;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * Hàm truy vấn trả về mảng các bản ghi
     * @param string $query
     * @return array 
     */
    function queryReturnArray($query) {
        try {
            $result = mysql_query($query);
            $resultArray = array();
            if ($result) {
                while ($row = mysql_fetch_assoc($result)) {
                    $resultArray[] = $row;
                }
            }
            return $resultArray;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * Hàm thực hiện 1 truy vấn. Nếu thành công trẻ về TRUE, ngược lại trả về FALSE
     * @param string $query
     * @return Boolean 
     */
    function query($query) {
        try {
            $result = mysql_query($query);
            return $result;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * Hàm thực hiện truy vấn trả về số lượng bản ghi bị ảnh hưởng.
     * Nếu thành công trả về số lượng bản ghi bị ảnh hưởng, ngược lại trả về 0
     * @param string $query
     * @return int 
     */
    function queryReturnAffectedRows($query) {
        $affectedrows = 0;
        try {
            $result = mysql_query($query);
            if ($result)
                $affectedrows = mysql_affected_rows();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        return $affectedrows;
    }

    /**
     * Hàm thực hiện xóa 1 bản ghi trong bảng bất kỳ theo Id.Nếu thành công trả về TRUE, ngược lại trả về FALSE
     * @param string $nametable:Tên bảng
     * @param string $nameid: Tên trường khóa chính của bảng
     * @param int $id
     * @return Boolean 
     */
    function deleteRecordById($nametable, $nameid, $id) {
        $query = "DELETE FROM " . $nametable . " WHERE " . $nameid . " = " . $id;
        try {
            $result = mysql_query($query);
            return $result;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * Hàm thực hiện xóa nhiều bản ghi cùng 1 lúc của 1 bảng bất kỳ theo Id
     * @param string $nametable: Tên bảng
     * @param string $nameid: Tên trường khóa chính của bảng
     * @param string $ids: Chuối Id có dạng: ví dụ: 1,2,3
     * @return Boolean 
     */
    function deleteMultiRecordByIds($nametable, $nameid, $ids) {
        $query = "DELETE FROM " . $nametable . " WHERE " . $nameid . " IN (" . $ids . ")";
        try {
            $result = mysql_query($query);
            return $result;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * Hàm trả về thông tin của 1 bản ghi trong bản bất kỳ
     * @param string $nametable:Tên bảng
     * @param string $nameid: Tên trường khóa chính của bảng
     * @param int $id
     * @return array 
     */
    function getInfoById($nametable, $nameid, $id) {
        $query = "SELECT * FROM " . $nametable . " WHERE " . $nameid . " = " . $id;
        try {
            $result = mysql_query($query);
            $resultArray = array();
            if ($result) {
                while ($row = mysql_fetch_assoc($result)) {
                    $resultArray[] = $row;
                }
            }
            return $resultArray;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * Hàm trả về tất cả các bản ghi trong 1 bảng bất kỳ và đưa vào mảng.
     * @param string $nametable:Tên bảng
     * @return array 
     */
    function getAllReturnArray($nametable) {
        $query = "SELECT * FROM " . $nametable . " ORDER BY id ASC";
        try {
            $result = mysql_query($query);
            $resultArray = array();
            if ($result) {
                while ($row = mysql_fetch_assoc($result)) {
                    $resultArray[] = $row;
                }
            }
            return $resultArray;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * Hàm thực hiện trả về N bản ghi đầu tiên hoặc cuối cùng trong 1 bảng bất kỳ và đưa vào mảng.
     * @param string $nametable: Tên bảng
     * @param string $nameid: Tên trường khóa chính của bảng.
     *        Nếu lấy N bản ghi đầu tiên thì $nameid=NULL
     *        Nếu lấy N bản ghi cuối cùng thì $nameid chính là tên khóa chính của bảng đó.
     * @param int $limit: Số lượng bản ghi đầu tiên cần lấy
     * @return array 
     */
    function getAllLimitReturnArray($nametable, $nameid, $limit) {
        $query = "";
        if ($nameid == NULL) {
            $query = "SELECT * FROM " . $nametable . " LIMIT " . $limit;
        } else {
            $query = "SELECT * FROM " . $nametable . " ORDER BY " . $nameid . " DESC LIMIT " . $limit;
        }
        try {
            $result = mysql_query($query);
            $resultarray = array();
            if ($result) {
                while ($row = mysql_fetch_assoc($result)) {
                    $resultarray[] = $row;
                }
            }
            return $resultarray;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * Hàm thực hiện trả về ID mới nhất sau khi thực hiện Insert 1 bản ghi.
     * @param string $query
     * @return int 
     */
    function getNewIdApterInsert($query) {
        try {
            mysql_query($query);
            $result = mysql_insert_id();
            return $result;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}

?>
